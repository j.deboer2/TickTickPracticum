﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

class CloudParalaxSpriteGameObject : SpriteGameObject
{
    double valueMultiplier;
    Random multiplier;

    public CloudParalaxSpriteGameObject(string assetName, int layer = 0, string id = "", int sheetIndex = 0) : base(assetName, layer, id, sheetIndex)
    {
        multiplier = new Random();
        valueMultiplier = multiplier.NextDouble(); //makes every mountain have a unique 'distance'
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
        if (!visible || sprite == null)
        {
            return;
        }
        sprite.Draw(spriteBatch, new Vector2(this.GlobalPosition.X - ((float)valueMultiplier * GameEnvironment.camera.Position.X), this.GlobalPosition.Y), origin);
    }
}

