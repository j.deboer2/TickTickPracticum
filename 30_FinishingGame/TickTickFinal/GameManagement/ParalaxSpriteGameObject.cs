﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

class ParalaxSpriteGameObject : SpriteGameObject
{
    double valueMultiplier;
    Random multiplier;
    
    public ParalaxSpriteGameObject(int multiplierLayer, string assetName, int layer = 0, string id = "", int sheetIndex = 0) : base(assetName, layer, id, sheetIndex)
    {
        multiplier = new Random();
        valueMultiplier = (multiplierLayer + multiplier.NextDouble()) / 10;
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
        if (!visible || sprite == null)
        {
            return;
        }
        sprite.Draw(spriteBatch, new Vector2(this.GlobalPosition.X - (float)valueMultiplier * GameEnvironment.camera.Position.X, this.GlobalPosition.Y), origin);
        
    }
}

