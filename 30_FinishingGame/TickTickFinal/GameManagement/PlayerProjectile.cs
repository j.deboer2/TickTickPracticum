﻿using Microsoft.Xna.Framework;

class PlayerProjectile : SpriteGameObject
{
    public PlayerProjectile(string assetName = "Sprites/spr_BombLeft", int layer = 0, string id = "", int sheetIndex = 0) : base(assetName, layer, id, sheetIndex)
    {
        velocity.X = 1000;
    }

    public override void Update(GameTime gameTime)
    {
        TileField tiles = GameWorld.Find("tiles") as TileField;
        base.Update(gameTime);
        GameObjectList enemies = GameWorld.Find("enemies") as GameObjectList;
        foreach (SpriteGameObject s in enemies.Children)
        {
            if (CollidesWith(s))
            {
                s.Visible = false;
                GameWorld.Remove(this);
                GameEnvironment.AssetManager.PlaySound("snd_bomb_explode");
            }
        }
        if (position.X < -Width || position.X > tiles.Columns * tiles.CellWidth)
        {
            GameWorld.Remove(this);
        }
    }

}

