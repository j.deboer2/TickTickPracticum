﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class PlayerVoice
{
    Random voiceLine = new Random();

    public void randomVoiceLine() //Picks and plays a random voice line
    {
        int voiceLineID = voiceLine.Next(0, 7);

        switch (voiceLineID)
        {
            default:
                GameEnvironment.AssetManager.PlaySound("snd_voice_bommetje");
                break;
            case 1:
                GameEnvironment.AssetManager.PlaySound("snd_voice_lontje");
                break;
            case 2:
                GameEnvironment.AssetManager.PlaySound("snd_voice_rotmuziek");
                break;
            case 3:
                GameEnvironment.AssetManager.PlaySound("snd_voice_sexbom");
                break;
            case 4:
                GameEnvironment.AssetManager.PlaySound("snd_voice_magikarp");
                break;
            case 5:
                GameEnvironment.AssetManager.PlaySound("snd_voice_auw");
                break;
            case 6:
                GameEnvironment.AssetManager.PlaySound("snd_voice_klikken");
                break;
        }
    }
}
