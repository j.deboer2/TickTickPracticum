﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

class StaticSpriteGameObject : SpriteGameObject
{
    public StaticSpriteGameObject(string assetName, int layer = 0, string id = "", int sheetIndex = 0) : base(assetName, layer, id, sheetIndex)
    {
    }

    public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
    {
        if (!visible || sprite == null)
        {
            return;
        }
        sprite.Draw(spriteBatch, this.GlobalPosition, origin);
    }
}

