﻿using Microsoft.Xna.Framework;

partial class Level : GameObjectList
{
    protected bool locked, solved;
    protected Button quitButton;
    protected int width;
    protected int height;
    protected int levelTimer;
    StaticSpriteGameObject backgroundSky = new StaticSpriteGameObject("Backgrounds/spr_sky");
    public int Height
    {
        get { return height; }
    }
    
    public int Width
    {
        get { return width; }
    }

    public Level(int levelIndex)
    {
        Add(new GameObjectList(1, "waterdrops"));
        Add(new GameObjectList(2, "enemies"));
        LoadTiles("Content/Levels/" + levelIndex + ".txt");

        // load the backgrounds
        GameObjectList backgrounds = new GameObjectList(0, "backgrounds");
        //SpriteGameObject backgroundSky = new SpriteGameObject("Backgrounds/spr_sky");
        backgroundSky.Position = new Vector2(0, GameEnvironment.Screen.Y - backgroundSky.Height);
        backgrounds.Add(backgroundSky);

        // add a few random mountains
        TileField tiles = GameWorld.Find("tiles") as TileField;
        for (int i = 0; i < 8; i++)
        {
            ParalaxSpriteGameObject mountain = new ParalaxSpriteGameObject(8 - i,"Backgrounds/spr_mountain_" + (GameEnvironment.Random.Next(2) + 1), 1);
            mountain.Position = new Vector2((float)GameEnvironment.Random.NextDouble() * (tiles.Columns * tiles.CellWidth) - mountain.Width / 2, 
                (tiles.Rows * tiles.CellHeight) - mountain.Height);
            backgrounds.Add(mountain);
        }

        Clouds clouds = new Clouds(tiles.Columns * tiles.CellWidth, 2);
        backgrounds.Add(clouds);
        Add(backgrounds);

        StaticSpriteGameObject timerBackground = new StaticSpriteGameObject("Sprites/spr_timer", 100, "timerBG");
        timerBackground.Position = new Vector2(10, 10);
        Add(timerBackground);
        

        quitButton = new Button("Sprites/spr_button_quit", 100, "quitBtn");
        quitButton.Position = new Vector2(GameEnvironment.Screen.X - quitButton.Width - 10, 10);
        Add(quitButton);

        TimerGameObject timer = new TimerGameObject(levelTimer, 101, "timer");
        timer.Position = new Vector2(25, 30);
        Add(timer);
    }

    public bool Completed
    {
        get
        {
            SpriteGameObject exitObj = Find("exit") as SpriteGameObject;
            Player player = Find("player") as Player;
            if (!exitObj.CollidesWith(player))
            {
                return false;
            }
            GameObjectList waterdrops = Find("waterdrops") as GameObjectList;
            foreach (GameObject d in waterdrops.Children)
            {
                if (d.Visible)
                {
                    return false;
                }
            }
            return true;
        }
    }

    public bool GameOver
    {
        get
        {
            TimerGameObject timer = Find("timer") as TimerGameObject;
            Player player = Find("player") as Player;
            return !player.IsAlive || timer.GameOver;
        }
    }

    public bool Locked
    {
        get { return locked; }
        set { locked = value; }
    }

    public bool Solved
    {
        get { return solved; }
        set { solved = value; }
    }
}

