﻿using Microsoft.Xna.Framework;

partial class Level : GameObjectList
{

    public override void HandleInput(InputHelper inputHelper)
    {
        base.HandleInput(inputHelper);
        if (quitButton.Pressed)
        {
            Reset();
            GameEnvironment.GameStateManager.SwitchTo("levelMenu");
        }      
    }

    public override void Update(GameTime gameTime)
    {
        base.Update(gameTime);

        TimerGameObject timer = Find("timer") as TimerGameObject;
        Player player = Find("player") as Player;
        TileField tiles = Find("tiles") as TileField;

        GameEnvironment.Camera.Position.X = player.Position.X - (GameEnvironment.Screen.X / 2);
        GameEnvironment.Camera.Position.Y = player.Position.Y - (GameEnvironment.Screen.Y / 2);

        if (GameEnvironment.camera.Position.X < 0)
        {
            GameEnvironment.camera.Position.X = 0;
        }
        else if (GameEnvironment.camera.Position.X >= width - GameEnvironment.Screen.X)
        {
            GameEnvironment.camera.Position.X = width - GameEnvironment.Screen.X;
        }
        if (GameEnvironment.camera.Position.Y >= (tiles.Rows * tiles.CellHeight) - GameEnvironment.Screen.Y)
        {
            GameEnvironment.camera.Position.Y = (tiles.Rows * tiles.CellHeight) - GameEnvironment.Screen.Y;
        }
        else if (GameEnvironment.camera.Position.Y <= GameEnvironment.Screen.Y - backgroundSky.Height)
        {
            GameEnvironment.camera.Position.Y = GameEnvironment.Screen.Y - backgroundSky.Height;
        }


   



        // check if we died
        if (!player.IsAlive)
        {
            timer.Running = false;
        }

        // check if we ran out of time
        if (timer.GameOver)
        {
            player.Explode();
        }
                       
        // check if we won
        if (Completed && timer.Running)
        {
            player.LevelFinished();
            timer.Running = false;
        }
    }

    public override void Reset()
    {
        base.Reset();
        VisibilityTimer hintTimer = Find("hintTimer") as VisibilityTimer;
        hintTimer.StartVisible();
    }
}
