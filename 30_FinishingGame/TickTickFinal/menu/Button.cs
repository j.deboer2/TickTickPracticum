﻿using Microsoft.Xna.Framework;

class Button : StaticSpriteGameObject
{
    protected bool pressed;

    public Button(string imageAsset, int layer = 0, string id = "")
        : base(imageAsset, layer, id)
    {
        pressed = false;
    }

    public override void HandleInput(InputHelper inputHelper)
    {
        Vector2 mousePosition = inputHelper.MousePosition;
        pressed = inputHelper.MouseLeftButtonPressed() &&
            BoundingBox.Contains((int)mousePosition.X, (int)mousePosition.Y);
    }

    public override void Reset()
    {
        base.Reset();
        pressed = false;
    }

    public bool Pressed
    {
        get { return pressed; }
    }
}
